/*
* Copyright 2017 zoomba-lang.org
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/

package org.zoomba.spark.impl;

import java.io.Serializable;
import java.util.List;
import scala.Tuple2;
import scala.collection.immutable.Seq;
import scala.reflect.ClassTag;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.interpreter.*;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.operations.ZJVMFunctionalInterface;
import zoomba.lang.core.types.ZNumber;

/**
 */
public class ScalaInteract implements Serializable {

    public static final String MY_NAME = "$_" ; // java $cala bridge

    public static ClassTag $tag(Class<?> c) {
        return scala.reflect.ClassTag$.MODULE$.apply(c);
    }

    public static final ClassTag $TAG = $tag(Object.class);

    public static final Function1 $I = Function1.IDENTITY;

    public static final Function2 $I2 = Function2.IDENTITY;

    public static <T> scala.collection.immutable.List<T> $l(List<T> javaList) {
        return scala.jdk.CollectionConverters.ListHasAsScala( javaList ).asScala().toList();
    }

    public static Tuple2 $t(Object o1, Object o2) {
        return new Tuple2(o1,o2);
    }

    public static Seq<?> $s(Object o){
        List<?> l = ZList.list(null,o);
        return $l(l);
    }

    public final String anonScript;

    public final boolean isMethod;

    public final String debugDump;

    private transient Function f;

    private Function function(){
        if ( f == null ){
            ZScript zs = new ZScript(anonScript);
            Function.MonadicContainer mc = zs.execute();
            ZScriptMethod zm = (ZScriptMethod) mc.value();
            f = zm.instance(new ZInterpret(zs));
        }
        return f;
    }

    public ScalaInteract(Object anon) {

        if ( anon instanceof Function) {
            Function f = (Function)anon ;
            anonScript = f.body() ;
            debugDump = f.toString();
            isMethod = !(f instanceof AnonymousFunctionInstance);
        }else if (anon instanceof ZJVMFunctionalInterface ) {
            Function f = ((ZJVMFunctionalInterface)anon).zmb();
            anonScript = f.body() ;
            debugDump = f.toString();
            isMethod = !(f instanceof AnonymousFunctionInstance);
        } else {
            anonScript = ";";
            isMethod = false ;
            debugDump = ";" ;
        }
    }

    Function.MonadicContainer safeCall(Object... args) {
        Function f = function();
        return f.execute(args);
    }

    public static Object f(Object... args) {
        if (args.length == 0) {
            return Function1.IDENTITY;
        }

        if (!(args[0] instanceof AnonymousFunctionInstance)) {
            return Function1.IDENTITY;
        }
        Object anon = args[0];
        int numArgs = ZNumber.integer(args[1], 0).intValue();
        switch (numArgs) {
            case 0:
                return new ScalaInteract.Function0(anon);
            case 1:
                return new ScalaInteract.Function1(anon);
            case 2:
                return new ScalaInteract.Function2(anon);
        }
        return Function1.IDENTITY;
    }

    public static AnonymousFunction.XFunction2 f2(Object... args) throws Exception {
        return new AnonymousFunction.XFunction2(  args[0] ) ;
    }

    public static class Function0 extends
            scala.runtime.AbstractFunction0 implements Serializable {

        public final ScalaInteract f;

        public Function0(Object a) {
            f = new ScalaInteract(a);
        }

        @Override
        public Object apply() {
            Function.MonadicContainer safe = f.safeCall();
            if ( safe.isNil()  ) {
                throw new Error("Error applying Function0");
            }
            return safe.value();
        }
    }


    public static class Function1 extends
            scala.runtime.AbstractFunction1 implements Serializable {

        public static final Identity IDENTITY = new Identity();

        public final ScalaInteract f;

        public Function1(Object a) {
            f = new ScalaInteract(a);
        }

        @Override
        public Object apply(Object o) {
            Function.MonadicContainer safe = f.safeCall(o);
            if ( safe.isNil() ) {
                throw new Error("Error applying Function1");
            }
            return safe.value() ;
        }

        public static final class Identity extends Function1 {
            private Identity() {
                super(null);
            }

            @Override
            public Object apply(Object o) {
                return o;
            }
        }
    }

    public static class Function2 extends
            scala.runtime.AbstractFunction2 implements Serializable {

        public static final Identity IDENTITY = new Identity();

        public final ScalaInteract f;

        public Function2(Object a) {
            f = new ScalaInteract(a);
        }

        @Override
        public Object apply(Object o, Object o2) {
            Function.MonadicContainer safe = f.safeCall(o, o2);
            if ( safe.isNil() ) {
                throw new Error("Error applying Function2");
            }
            return safe.value();
        }

        public static final class Identity extends Function2 {
            private Identity() {
                super(null);
            }

            @Override
            public Object apply(Object o, Object o2) {
                return new Object[]{o, o2};
            }
        }
    }
}
