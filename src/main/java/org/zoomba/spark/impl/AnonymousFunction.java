/**
 * Copyright 2017 zoomba-lang.org
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.zoomba.spark.impl;

import org.apache.spark.api.java.function.*;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;
import zoomba.lang.core.types.ZException;
import zoomba.lang.core.operations.Function.MonadicContainer;

import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.List;

/**
 */
public class AnonymousFunction extends ScalaInteract {

    public AnonymousFunction(Object anon){
        super(anon);
    }

    public static final class XVoidFunction extends AnonymousFunction
            implements VoidFunction{

        public XVoidFunction(Object a){
            super(a);
        }

        @Override
        public void call(Object o) throws Exception {
            MonadicContainer ret = safeCall(o);
            if ( ret.value() instanceof ZException ) throw new Exception("XFunction has thrown exception!");
        }
    }

    public static final class XFunction extends AnonymousFunction
            implements Function{

        public XFunction(Object a){
            super(a);
        }

        @Override
        public Object call(Object v1) throws Exception {
            MonadicContainer ret = safeCall(v1);
            if ( ret.value() instanceof Throwable ) throw new Exception("XFunction has thrown exception!");
            return ret.value();
        }
    }

    public static final class XFunction2 extends AnonymousFunction
            implements org.apache.spark.api.java.function.Function2{

        public XFunction2(Object a){
            super(a);
        }

        @Override
        public Object call(Object v1, Object v2) throws Exception {
            MonadicContainer ret  = safeCall(v1,v2);
            if ( ret.value() instanceof Throwable) throw new Exception("XFunction2 has thrown exception!");
            return ret.value();
        }
    }

    public static final class XDoubleFunction extends AnonymousFunction
            implements DoubleFunction {
        public XDoubleFunction(Object a){
            super(a);
        }

        @Override
        public double call(Object o) throws Exception {
            MonadicContainer  safe = safeCall(o);
            if ( safe.value() instanceof Number ){
                return ((Number) safe.value()).doubleValue();
            }
            throw new Exception("XDoubleFunction failed!");
        }
    }

    public static final class XPairFunction extends AnonymousFunction implements PairFunction{

        public XPairFunction(Object a){
            super(a);
        }

        @Override
        public Tuple2 call(Object o) throws Exception {
            MonadicContainer sc = safeCall(o);
            if ( !sc.isNil() ){
                Object ret = sc.value();
                Object l,r;
                if ( ret.getClass().isArray() ){
                    l = Array.get(ret,0);
                    r = Array.get(ret,1);
                    return new Tuple2(l,r);
                }
                if ( ret instanceof List){
                    l = ((List) ret).get(0);
                    r = ((List) ret).get(1);
                    return new Tuple2(l,r);
                }
            }
            throw new Exception("PairFunction throw error!");
        }
    }

    public static final class XFlatMapFunction extends AnonymousFunction
            implements FlatMapFunction,PairFlatMapFunction, DoubleFlatMapFunction {

        public XFlatMapFunction(Object a){
            super(a);
        }

        @Override
        public Iterator call(Object o) throws Exception {
            MonadicContainer r = safeCall(o);
            if ( r.value() instanceof Iterable ) return  ((Iterable) r.value()).iterator();
            throw new Exception("FlatMapFunction throw error!");
        }
    }


}
