/**
 * Copyright 2017 zoomba-lang.org
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.zoomba.spark;

import org.apache.spark.SparkContext;
import org.zoomba.spark.impl.ScalaInteract;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * A Main class to have command line interface, if need be.
 */
public class Main {


    private static final FlatMapFunction<String, String> WORDS_EXTRACTOR =
            s -> Arrays.asList(s.split(" ")).listIterator();

    private static final PairFunction<String, String, Integer> WORDS_MAPPER =
            s -> new Tuple2<String, Integer>(s, 1);

    private static final Function2<Integer, Integer, Integer> WORDS_REDUCER =
            Integer::sum;


    private Main() {
    }

    public static void ain(String[] args) {

        String inputFileName = "samples/big.txt" ;
        String outputDirName = "output" ;

        SparkConf conf = new SparkConf().setAppName("org.sparkexample.WordCount").setMaster("local");
        JavaSparkContext context = new JavaSparkContext(conf);

        JavaRDD<String> file = context.textFile(inputFileName);
        JavaRDD<String> words = file.flatMap(WORDS_EXTRACTOR);
        JavaPairRDD<String, Integer> pairs = words.mapToPair(WORDS_MAPPER);
        JavaPairRDD<String, Integer> counter = pairs.reduceByKey(WORDS_REDUCER);

        counter.saveAsTextFile(outputDirName);
    }

    public static Object executeScript(String script) throws Throwable {
        Map<String,Object> binding = new HashMap<>();
        binding.put("spark", XSpark.class );
        binding.put( ScalaInteract.MY_NAME , ScalaInteract.class );
        ZScript s = new ZScript(script,null);
        Function.MonadicContainer mc = s.eval(binding);
        try {
            if (mc.value() instanceof Throwable) {
                throw (Throwable) ((Throwable) mc.value()).getCause();
            }
            return mc.value();
        }finally {
            SparkContext ctx = (SparkContext)binding.get("context");
            if ( ctx != null ) { // could load stuff
                ctx.stop();
            }
        }
    }

    public static void main(String[] args) throws Throwable {
        executeScript(args[0]);
    }
}
